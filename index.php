<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/styles/buttons.css" type="text/css" rel="stylesheet" />
		<title>9iron</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="iconbuttoncontainer">
				<a href="/minecraft" class="iconbutton">
					<i class="fa fa-cube"></i>
					<h1>Minecraft</h1>
					<p>Modded Minecraft is one of our most-common time wasters</p>
				</a>
				<a href="/terraria" class="iconbutton">
					<i class="fa fa-tree"></i>
					<h1>Terraria</h1>
					<p>A game we've sunk just as much time into as Minecraft. Sometimes modded, sometimes not</p>
				</a>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
