<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/errors/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>500 Internal Server Error</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>@w@</h1>
				<h2>500</h2>
				<p>Something bad happened. Contact the site administrator and mention the error "500", the URL you tried to access, and the time it happened.</p>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
