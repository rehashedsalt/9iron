<div class="footer">
	<p>Copyright &copy 2019-<?php echo date("Y"); ?> rehashedsalt. Distributed under the terms of the MIT License.</p>
	<p><a href="https://git.desu.ltd/salt/9iron">source</a> <a href="/">home</a></p>
</div>
