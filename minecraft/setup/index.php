<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/minecraft/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>9iron - Modded Minecraft Setup</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<a href="/minecraft" class="startbutton">
					<i class="fa fa-arrow-left"></i>
					<p>Return to modpacks</p>
				</a>
				<h1>Setting Up Modded Minecraft</h1>
			</div>
			<div class="subcontainer" id="foreword">
				<h2>0. Sanity Check</h2>
				<p>This guide will only work under the following conditions:</p>
				<ul>
					<li>You own Minecraft: Java Edition</li>
					<li>You log in with a Mojang account, NOT a Microsoft account</li>
				</ul>
				<p>If these aren't true for you, stop reading here and consult with one of your server admins.</p>
			</div>
			<div class="subcontainer" id="java">
				<h2>1. Installing Java</h2>
				<p>Minecraft depends on Java. It's important that you have the latest 64-bit edition of Java installed.</p>
				<ol>
					<li>Go to <a href="https://adoptium.net/">Adoptium</a></li>
					<li>Select "OpenJDK 8"</li>
					<li>Click "Latest release"</li>
					<li>Run through the installer</li>
				</ol>
			</div>
			<div class="subcontainer" id="multimc">
				<h2>2. Installing MultiMC</h2>
				<p>MultiMC is the launcher that we use to organize our modpacks. It has a number of features to make pack installation easier.</p>
				<ol>
					<li>Download <a href="https://multimc.org/#Download">MultiMC</a></li>
					<li>Extract the zip file somewhere, like your desktop or Documents folder</li>
					<li>Run <code>multimc.exe</code></li>
				</ol>
				<p>MultiMC will then run you through basic setup. You can accept the defaults, but will probably want to change the following:</p>
				<ul>
					<li>On the page where it asks you to set up Java, set these values:</li>
					<ul>
						<li>Set "Maximum Memory Allocation" to 4096MB</li>
						<li>Set "Minimum Memory Allocation" to 1024MB</li>
					</ul>
				</ul>
				<p>After that, it'll drop you to the main screen and you can progress to the next step.</p>
			</div>
			<div class="subcontainer" id="pack">
				<h2>3. Installing a Modpack</h2>
				<p>All modpacks here are installable through MultiMC.</p>
				<ol>
					<li>Download the modpack you want to play</li>
					<li>Open MultiMC</li>
					<li>Hit the "Add Instance" button in the upper-left</li>
					<li>Pick the option on the left called "Import from Zip"</li>
					<li>Browse for the zip you just downloaded</li>
					<li>Click OK</li>
				</ol>
				<p>This will set you up with a nice, shiny new instance with all the mods ready to go. Double-click it to launch it.</p>
				<p>If MultiMC prompts for it, be sure to set up your Minecraft account.</p>
			</div>
			<div class="subcontainer" id="joining">
				<h2>4. Joining the Server</h2>
				<p>Now the last thing we need to do is connect you to the server.</p>
				<ol>
					<li>Launch the game by double-clicking the modpack icon</li>
					<li>Copy the server URL (something like <code>modpack.mc.9iron.club</code>)</li>
					<li>In the game, go to "Multiplayer", and click "Add Server"</li>
					<li>Stick whatever name you want in the name field and paste the server URL in the "IP Address" field</li>
					<li>Double-click it to join</li>
				</ol>
				<p>You're looking for the huge, can't-miss-it serif font with the gray background:</p>
				<code class="bigcode">serverip.blah.blah</code>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
		</div>
	</body>
</html>
