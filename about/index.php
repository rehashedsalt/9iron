<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<title>9iron - About</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>About 9iron</h1>
				<p>9iron is a website spun up by yours truly to unify the services I host and inevitably rope my friends into. It also aggregates documentation about the various games we play.</p>
				<h1>About Me</h1>
				<p>I am Salt. I'm an SRE and I play video games. You can reach me effectively through Discord if you have my handle, otherwise email is the best way to get ahold of me.</p>
				<dl>
					<dt>Email:</dt>
					<dd>Check my commits</dd>
				</dl>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
