<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link rel="shortcut icon" href="/favicon.ico" />
		<title>9iron</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>Terraria</h1>
				<p>This game just keeps getting fucking updates, dude. Listed here are all the servers that we have going at the moment.</p>
				<p>To join, just install tModLoader through Steam (you have to own Terraria) and connect through the Multiplayer menu. You'll automatically download all the mods the server uses.</p>
				<p>The password is <code>dicks</code>. The password is always <code>dicks</code>.</p>
			</div>
			<div class="section">
				<h1>Current Playthroughs</h1>
				<p>Ain't nobody here but us chickens</p>
			</div>
			<div class="section">
				<h1>Old Playthroughs</h1>
				<div class="subcontainer">
					<h2>Calamity</h2>
					<p>tModLoader 0.11.7.5 (latest Steam)</p>
					<p>A pretty light modpack (as Terraria packs tend to be) with Calamity, extra music, and Magic Storage. Also the 1.4 door tweak thing, that's important too.</p>
					<p>World Backups:</p>
					<ul>
						<li><a href="https://srv.9iron.club/files/calamity1.tgz">calamity1</a> - 2020-10-05</li>
					</ul>
				</div>
				<div class="subcontainer">
					<h2>1.4 Launch</h2>
					<p>It totally met the hype, but was simultaneously underwhelming. I don't know how they did it.</p>
					<p>World Backups:</p>
					<ul>
						<li><a href="https://srv.9iron.club/files/1401-farm-master1.wld">1401-farm-master1</a> - 2020-06-01</li>
					</ul>
				</div>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
