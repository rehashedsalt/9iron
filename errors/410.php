<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/errors/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>410 Gone</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>...</h1>
				<p>Whatever's here isn't anymore.</p>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
