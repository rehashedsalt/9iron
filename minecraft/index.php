<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/styles/buttons.css" type="text/css" rel="stylesheet" />
		<link href="/minecraft/styles/styles.css" type="text/css" rel="stylesheet" />
		<link rel="shortcut icon" href="/favicon.ico" />
		<title>9iron - Modded Minecraft</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<a href="/minecraft/setup" class="startbutton">
					<i class="fa fa-arrow-right"></i>
					<p>Confused? Click here to get started</p>
				</a>
			</div>
			<?php
# Define a pack card generator function
function echo_pack($pack) {
	# Add basic coloration to the element, if it exists
	if (file_exists("packs/$pack/color")) {
		$color = file_get_contents("packs/$pack/color");
		echo "<div class=\"modpack\" style=\"background: $color; color: var(--background);\">";
	} else {
		echo '<div class="modpack">';
	}

	# Push an image if we've got one of those
	if (file_exists("packs/$pack/icon.png")) {
		echo "<img src=\"/minecraft/packs/$pack/icon.png\" class=\"packicon\"/>";
	}

	# Get the pack description
	$desc = "<h2>$pack</h2><p>A modpack.</p>";
	$descfile = "packs/$pack/desc.html";
	if (file_exists($descfile)) {
		$desc = file_get_contents($descfile);
	}
	echo "$desc";

	# A small button to browse previous world downloads
	echo "<p><a href=\"https://srv.9iron.club/files/packs/$pack/worlds\" class=\"worldsbutton\">Browse world backups</a></p>";

	# And a bright big download button
	echo "<a href=\"https://srv.9iron.club/files/packs/$pack/latest.zip\" class=\"downloadbutton\">Download</a>";
	echo '</div>';
}
# Get list of modpacks
$dir = new DirectoryIterator("packs");
$packs = array();
foreach ($dir as $fileinfo) {
	if (!$fileinfo->isDot() && $fileinfo->isDir()) {
		array_push($packs, $fileinfo->getFilename());
	}
}

# Parse through and provide their information
if (!empty($packs)) {
	echo '<div class="section">';
	# If we have a current pack, print that upfront
	if (file_exists("packs/current") && readlink("packs/current")) {
		# We have a valid current pack, remove it from the list and print it upfront
		$currentpack = readlink("packs/current");
		$packs = array_diff($packs, ["$currentpack", "current"]);
		echo '<div class="currentcontainer"><h1>Current Modpack</h1>';
		echo_pack($currentpack);
		# If there's a server IP name along with that, print that out too
		if (file_exists("packs/currentip")) {
			$ip = file_get_contents("packs/currentip");
			echo "<p><code class=\"bigcode\">$ip</code></p>";
		}
		echo '</div></div><div class="section">';
	}
	# Sort the list of remaining packs
	sort($packs);
	# Print the rest of them
	echo '<h1>Modpacks</h1><div class="packcontainer">';
	foreach ($packs as $pack) {
		echo_pack($pack);
	}
	echo '</div></div>';
}
			?>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
