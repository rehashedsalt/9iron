<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/errors/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>403 Forbidden</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>&gt;w&lt;</h1>
				<h2>403</h2>
				<p>You don't have permission to access this resource.</p>
				<p>If you think this is in error, contact the site administrator.</p>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
