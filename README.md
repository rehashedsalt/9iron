# 9iron

A website for me and my friends.

## Installation

This repo is a ready-to-go webroot. `git clone` and configure Apache. `git pull` for updates.

## Docker

You can pull the image from `rehashedsalt/9iron`. The `:latest` tag will always have the latest git tag applied and the `:bleeding` tag will always be the latest *commit*.

The image listens on `:80` and does not terminate SSL; proxy connections if you're interested in that.

## Random Subtitles

I've got random subtitles generated at page load. Hit me up (i.e. open an issue) if you've got dumb ideas for some.
